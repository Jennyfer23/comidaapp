package com.example.comidaappjuchique

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    private val comidas = mutableListOf<Comida>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        addComida()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun addComida() {
        comidas.add(Comida("Antojitos olga",19.841041071111537,-96.69512785009242))
        comidas.add(Comida("Comida lucia",19.841295388009588,-96.69517505068862))
        comidas.add(Comida("Antojitos chela", 19.84113795339875,-96.69375903083808))
        comidas.add(Comida("Antojitos y tamales licha", 19.840738311561875,-96.69584444182522))
        comidas.add(Comida("Tamales y chocomiles", 19.841085475160458,-96.69425249230115))
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        for (comida in comidas){
            val comidaPosition = LatLng(comida.latitud,comida.longitud)
            val comidaName = comida.name

            val markerOptions = MarkerOptions().position(comidaPosition).title(comidaName)
            mMap.addMarker(markerOptions)
        }

        // Add a marker in Sydney and move the camera
        val kiosko = LatLng(19.841271167582843, -96.69472020801423)

        mMap.addMarker(MarkerOptions().position(kiosko).title("Marker in kiosko"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(kiosko))
    }
}